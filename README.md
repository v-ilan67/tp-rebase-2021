4 - Essayez de pusher votre branche sur le remote origin. Que constatez-vous ?
Ca ne marche pas 
$ git push --set-upstream origin 1-customize-readme
remote: You are not allowed to push code to this project.
fatal: unable to access 'https://gitlab.com/fnuttens/tp-rebase-2021.git/': The requested URL returned error: 403

Nous n’avons pas les droits car nous sommes uniquement des LECTEURS. Il faudrait que l’origin donc fnuttens nous rajoute dans son projet en tant que contributeur. L’alternative est de push en personnal.

5 - Que constatez-vous à propos de vos branches ? De votre issue ?
Mon issue se ferme automatiquement car il y a un Closes#1 et la branche est supprimée.
